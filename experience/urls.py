from django.conf.urls import url
from . import views

app_name = 'experience'

urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^(?P<job_id>\d+)/$', views.detail, name='detail')
]
