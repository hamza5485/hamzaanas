from django.core import serializers
from django.http import Http404, JsonResponse
from django.views import generic

# Create your views here.
from experience.models import Job, Company


class IndexView(generic.ListView):
    template_name = 'experience/index.html'

    def get_queryset(self):
        return Job.objects.all().order_by('-date_from')

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context['active'] = 'experience'
        # context['tagline'] = 'EXPERIENCE'
        return context


def detail(request, job_id):
    try:
        job = Job.objects.filter(pk=job_id)
        company = Company.objects.filter(pk=job[0].comp_id)
        context = {}
        if request.is_ajax():
            context['function'] = '[{"function_name": "experience"}]'
            context['job'] = serializers.serialize("json", job)
            context['company'] = serializers.serialize("json", company)
            return JsonResponse(context)
        else:
            raise Http404
    except Job.DoesNotExist:
        raise Http404
