from django.db import models


# Create your models here.
class Company(models.Model):
    name = models.CharField(max_length=100)
    image = models.FileField()
    url = models.URLField(max_length=200)
    location = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Job(models.Model):
    title = models.CharField(max_length=100)
    date_from = models.DateField('From')
    date_to = models.DateField('To')
    description = models.TextField()
    comp = models.ForeignKey(Company, models.SET_NULL, blank=True, null=True)

    def __str__(self):
        return self.title
