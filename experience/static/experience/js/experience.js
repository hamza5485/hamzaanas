function experience(data) {
    let json_job = JSON.parse(data.job);
    let json_company = JSON.parse(data.company);
    let job = json_job[0].fields;
    let company = json_company[0].fields;
    $(".detail-img").attr("src", "/media/" + company.image);
    $("#title").html(job.title);
    $("#company").html('<a href="' + company.url + '" target="blank">' + company.name + '</a>');
    $("#location").html(company.location);
    $("#from").html(moment(job.date_from, "YYYY-MM-DD").format("MMMM, YYYY"));
    $("#to").html(moment(job.date_to, "YYYY-MM-DD").format("MMMM, YYYY"));
    $("#first-half").empty();
    $("#second-half").empty();
    $('#description').html(job.description);
    $('.modal').modal('show');
}