from django.shortcuts import render

# Create your views here.
from django.views import generic

from skill.models import Skill


class IndexView(generic.ListView):
    template_name = 'skill/index.html'

    def get_queryset(self):
        return Skill.objects.all()

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context['active'] = 'skill'
        # context['tagline'] = 'SKILLS'
        print(context)
        return context
