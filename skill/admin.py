from django.contrib import admin
from skill.models import *

# Register your models here.

admin.site.register(Specialization)
admin.site.register(Language)
admin.site.register(Tool)
admin.site.register(Skill)
