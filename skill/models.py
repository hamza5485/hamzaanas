from django.db import models


# Create your models here.
class Specialization(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class Tool(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class Language(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class Skill(models.Model):
    name = models.CharField(max_length=50)
    specialization = models.ForeignKey(Specialization, models.SET_NULL, blank=True, null=True)
    language = models.ManyToManyField(Language, related_name='language')
    tools = models.ForeignKey(Tool, models.SET_NULL, blank=True, null=True)

    def __str__(self):
        return self.name
