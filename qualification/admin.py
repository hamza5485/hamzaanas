from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(School)
admin.site.register(Degree)
admin.site.register(Course)
admin.site.register(CertBody)
admin.site.register(Certification)
