from django.apps import AppConfig


class QualificationsConfig(AppConfig):
    name = 'qualification'
