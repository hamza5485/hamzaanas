from django.db import models


# Create your models here.
class School(models.Model):
    name = models.CharField(max_length=100)
    img = models.FileField()
    url = models.URLField(max_length=200)
    location = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Degree(models.Model):
    DEGREE_LEVEL_CHOICES = (
        ('High School', 'High School'),
        ('College', 'College'),
        ('Bachelors', 'Bachelors'),
        ('Masters', 'Masters')
    )
    name = models.CharField(max_length=100)
    level = models.CharField(max_length=50, choices=DEGREE_LEVEL_CHOICES)
    url = models.URLField(max_length=200)
    date_from = models.DateField('Year Started')
    date_to = models.DateField('Year Finished')
    school = models.ForeignKey(School, models.SET_NULL, blank=True, null=True)

    def __str__(self):
        return self.name


class Course(models.Model):
    name = models.CharField(max_length=200)
    code = models.CharField(max_length=10)
    url = models.URLField(max_length=200)
    degree = models.ForeignKey(Degree, models.SET_NULL, blank=True, null=True)

    def __str__(self):
        return self.code + " - " + self.name


class CertBody(models.Model):
    name = models.CharField(max_length=100)
    url = models.URLField(max_length=200)

    def __str__(self):
        return self.name


class Certification(models.Model):
    title = models.CharField(max_length=200)
    year = models.DateField('Year Completed')
    url = models.URLField(max_length=200)
    provider = models.ForeignKey(CertBody, models.SET_NULL, blank=True, null=True)
    school = models.ForeignKey(School, models.SET_NULL, blank=True, null=True)

    def __str__(self):
        return self.title
