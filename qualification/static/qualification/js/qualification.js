function qualification(data) {
    let json_degree = JSON.parse(data.degree);
    let json_school = JSON.parse(data.school);
    let courses = JSON.parse(data.course);
    let degree = json_degree[0].fields;
    let school = json_school[0].fields;
    $(".detail-img").attr("src", "/media/" + school.img);
    $("#degree").html('<a href="' + degree.url + ' " target="_blank">' + degree.name + '</a>');
    $("#level").html(degree.level);
    $("#institution").html('<a href="' + school.url + '" target="blank">' + school.name + '</a>');
    $("#location").html(school.location);
    $("#from").html(moment(degree.date_from, "YYYY-MM-DD").format("MMMM, YYYY"));
    $("#to").html(moment(degree.date_to, "YYYY-MM-DD").format("MMMM, YYYY"));
    $("#first-half").empty();
    $("#second-half").empty();
    for (let i in courses) {
        let course = courses[i].fields;
        if (i <= courses.length / 2) {
            $("#first-half").append('<a href="' + course.url + '" target="_blank" class="list-group-flush detail-list">'
                + course.code + " " + course.name + '</a>');
        } else {
            $("#second-half").append('<a href="' + course.url + '" target="_blank" class="list-group-flush detail-list">'
                + course.code + " " + course.name + '</a>');
        }
    }
    $('.modal').modal('show');
}