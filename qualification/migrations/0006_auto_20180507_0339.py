# Generated by Django 2.0.5 on 2018-05-06 17:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('qualification', '0005_auto_20180507_0326'),
    ]

    operations = [
        migrations.AlterField(
            model_name='school',
            name='img',
            field=models.ImageField(upload_to='qualification/images/'),
        ),
    ]
