# Generated by Django 2.0.5 on 2018-05-06 16:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('qualification', '0003_auto_20180507_0237'),
    ]

    operations = [
        migrations.AlterField(
            model_name='degree',
            name='level',
            field=models.CharField(choices=[('High School', 'High School/O Levels'), ('College', 'College/A Levels'), ('BSc', 'Bachelors of Science'), ('M', 'Masters')], max_length=50),
        ),
    ]
