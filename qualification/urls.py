from django.conf.urls import url
from . import views

app_name = 'qualification'

urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^(?P<degree_id>\d+)/$', views.detail, name='detail'),
]
