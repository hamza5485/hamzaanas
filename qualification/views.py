from django.core import serializers
from django.http import Http404, JsonResponse
from django.views import generic
from qualification.models import Degree, School, Course, Certification


# Create your views here.
class IndexView(generic.ListView):
    template_name = 'qualification/index.html'

    def get_queryset(self):
        return Degree.objects.all().order_by('-date_to')

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context['certifications'] = Certification.objects.all().prefetch_related()
        context['active'] = 'qualification'
        # context['tagline'] = 'QUALIFICATIONS'
        return context

    # class DetailView(generic.DetailView):
    #     model = Degree
    #     template_name = 'qualification/detail.html'


def detail(request, degree_id):
    try:
        degree = Degree.objects.filter(pk=degree_id)
        school = School.objects.filter(pk=degree[0].school_id)
        course = Course.objects.filter(degree_id=degree_id)
        context = {}
        if request.is_ajax():
            context['function'] = '[{"function_name": "qualification"}]'
            context['degree'] = serializers.serialize("json", degree)
            context['school'] = serializers.serialize("json", school)
            context['course'] = serializers.serialize("json", course)
            return JsonResponse(context)
        else:
            raise Http404
    except Degree.DoesNotExist:
        raise Http404
