/* for formatting dates on secondary cards */
let text = $(".list-group-item .card-subtitle .text-muted").text();
$(".list-group-item .card-subtitle .text-muted").text(moment(text, "YYYY-MM-DD").format("MMMM, YYYY"));


$(".primary").click(function () {
    $(".detail-table").hide();
    $("hr.slide").hide();
    changeButtonText($(".view-detail").attr('data-active'), "close");
    let url = $(this).attr("data-url");
    let id = $(this).attr("data-id");
    $.ajax({
        type: 'GET',
        url: url,
        data: id,
        success: function (data) {
            let fx = String(JSON.parse(data.function)[0].function_name);
            switch (fx) {
                case "qualification":
                    qualification(data);
                    break;
                case "experience":
                    experience(data);
                    break;
            }
        }
    });
});


$(".view-detail").click(function () {
    let visibility = $(".detail-table").css("display");
    let active = $(".view-detail").attr('data-active');
    console.log(active);
    if (visibility == "none") {
        $(".detail-table").slideDown();
        $("hr.slide").slideDown();
        changeButtonText(active, "open");
    } else {
        $(".detail-table").slideUp();
        $("hr.slide").slideUp();
        changeButtonText(active, "close");
    }
});

function changeButtonText(active, state) {
    if (state == "open") {
        switch (active) {
            case "qualification":
                $(".view-detail").text("Hide Courses");
                break;
            case "experience":
                $(".view-detail").text("Hide Description");
                break;
        }
    } else {
        switch (active) {
            case "qualification":
                $(".view-detail").text("View Courses");
                break;
            case "experience":
                $(".view-detail").text("View Description");
                break;
        }
    }
}